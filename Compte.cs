using client.Mada;
public class Compte
{
    //encapsulation
    public float Solde { get; private set; }
    public uint NumCompte { get; private set; }
    public Client Proprio { get; set; }
    //comptage compte
    private static uint s_compteur = 0; 

    // Constructeur par défaut
    public Compte()
    {
        Solde = default; // Initialisation du solde avec la valeur par défaut du type float (0.0)
        NumCompte = ++s_compteur; // Incrémentation du compteur et attribution du code du compte
        Proprio = new(); // Initialisation du propriétaire avec un nouvel objet Client
    }

    // Constructeur avec un paramètre proprio (propriétaire du compte)
    public Compte(Client proprio) :
        this() // Appel du constructeur par défaut
    {
        Proprio = proprio ?? new(); // Attribution du propriétaire ou création d'un nouvel objet Client
    }

    // Constructeur avec deux paramètres solde et proprio
    public Compte(float solde, Client proprio) :
        this(proprio) // Appel du constructeur avec un paramètre proprio
    {
        Solde = solde; // Attribution du solde
    }

    // Méthode pour créditer le compte
    public void Crediter(float montant)
    {
        Solde += montant; // Ajout du montant au solde
    }

    // Méthode pour créditer le compte et débiter un autre compte
    public void Crediter(float montant, Compte crediteur)
    {
        Crediter(montant); // Appel de la méthode Crediter(float montant) pour créditer le compte actuel
        crediteur.Debiter(montant); // Appel de la méthode Debiter(float montant) pour débiter l'autre compte
    }

    // Méthode pour débiter le compte
    public void Debiter(float montant)
    {
        Solde -= montant; // Retrait du montant du solde
    }

    // Méthode pour débiter le compte et créditer un autre compte
    public void Debiter(float montant, Compte debiteur)
    {
        Debiter(montant); // Appel de la méthode Debiter(float montant) pour débiter le compte actuel
        debiteur.Crediter(montant); // Appel de la méthode Crediter(float montant) pour créditer l'autre compte
    }

    // Méthode pour afficher les informations du compte
    public string Afficher()
    {
        // Utilisation de la syntaxe $@" pour créer une chaîne avec des retours à la ligne
        return $@"
        ************************
        Numéro de Compte: {NumCompte}
        Solde de compte: {Solde}
        Propriétaire du compte :
        {Proprio.Afficher()}
        ************************";
    }

    // Méthode statique pour obtenir le nombre total de comptes créés
    public static string NbCompte()
    {
        return $"Le nombre de comptes créés: {s_compteur}";
    }
    
}
