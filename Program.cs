﻿using System;
using System.ComponentModel.Design;
using System.Collections.Generic;
namespace TpExoCsharp;
using System.Security.Cryptography.X509Certificates;
using client.Mada;
using GestionJoueurs;
using MancheJeu;
using ClockManaging;
using ConsoleManaging;
using System.Threading.Tasks;
using System.Threading;



class Program
{
    static List<Compte> listeDesComptes = new List<Compte>(); 
    static int nombreDeComptesCrees = 0;  
    static void Main(string[] args)
    {
        
        while (true)
        {
            Menu();

        Console.Write("Entrer votre choix : ");
        string choix = Console.ReadLine();

        if(choix == "1")
        {
            Banque();
        }
        else if (choix == "2")
        {
            Cercle();
        }
        else if (choix == "3")
        {
            JeuxD();
        }
        else if (choix == "4")
        {
            Horloge();
        }
        else if (choix == "0")
        {
            //Revenir au menu principal
            continue;
        }
        else if (choix == "9"){
            //Quitte le program
            break;
        }
        else 
        {
            Console.WriteLine ("Choix non valide");

        }
        

    }
}
    static void Cercle(){

        Console.Write("Donner l'abscice du centre : ");
        double x=double.Parse(Console.ReadLine());

        Console.Write("Donner l'ordonner du centre : ");
        double y=double.Parse(Console.ReadLine());

        Console.Write("Donner le rayon: ");
        double r=double.Parse(Console.ReadLine());

        Cercle point = new Cercle((double)x,(double)y,(double)r);
        point.Display();
        point.Perimetre();
        point.Surface();

        Console.WriteLine("Donner un point : ");
        Console.Write("x: ");
        double monx =double.Parse(Console.ReadLine());
        Console.Write("y:");
        double mony = double.Parse(Console.ReadLine());
        
    }

    static void Menu(){
        Console.WriteLine("\nMENU PRINCIPALE");
        Console.WriteLine("___________________");
        Console.WriteLine("1.Compte banque");
        Console.WriteLine("2.Cercle");
        Console.WriteLine("3.Jeux de dés");
        Console.WriteLine("4.Horloge");
        Console.WriteLine("9.Quitter");
        Console.WriteLine("___________________");
    }
 
    static void Banque (){
        Console.WriteLine("___________________");
            Console.WriteLine("1.Commencer");
            Console.WriteLine("0.Menu principal");
        Console.WriteLine("___________________");
            Console.Write("Entrer votre choix : ");
            string choix3a = Console.ReadLine();
            
        if(choix3a == "1")
        { 
            Compte1();
        }
        else if(choix3a == "0")
        {
            Menu();
        }
        static void Compte1 ()
        {
            if(nombreDeComptesCrees == 0)
            {
            String cin, lastN, firstN, tel;
            //Compte1
            Console.WriteLine("___________________");
            Console.Write($"Compte 1 :");
            Console.Write("\nDonner Le CIN: ");
            cin = Console.ReadLine() ?? "";
            Console.Write($"Donner Le Nom: ");
            lastN = Console.ReadLine() ?? "";
            Console.Write($"Donner Le Prénom: ");
            firstN = Console.ReadLine() ?? "";
            Console.Write($"Donner Le numéro de télephone: ");
            tel= Console.ReadLine() ?? "";
            Console.WriteLine("___________________");
            

            //Creation compte client
            Compte cpt= new Compte(new Client(cin, firstN, lastN, tel));
            listeDesComptes.Add(cpt);
            nombreDeComptesCrees++;
            Console.WriteLine($"Détails du compte:{cpt.Afficher()}");
            Console.WriteLine("1.Creer un autre compte ");
            Console.WriteLine("2.Enter dans fonctionnalité");
            Console.WriteLine("0.Menu principal");
            Console.Write("Entrer votre choix : ");
            string choix3d = Console.ReadLine();
            if(choix3d == "1")
            {
                Compte();
            }
            else if(choix3d == "2")
            {
                choix();
            }
            else if(choix3d == "0")
            {
                Menu();
            }
            else
            {
                Console.WriteLine ("Choix non valide");
            }
            }

        }
        static void Compte() {
        Console.WriteLine("1.Enter le nombre de compte a creer :");
        int ? nbrCompte = int.Parse(Console.ReadLine());
        String cin, lastN, firstN, tel;
        for(int i=nombreDeComptesCrees;i<=nbrCompte;i++)
            { 
                        
            Console.Write($"Compte {nombreDeComptesCrees + 1} :");
            Console.Write("Donner Le CIN: ");
            cin = Console.ReadLine() ?? "";
            Console.Write($"Donner Le Nom: ");
            lastN = Console.ReadLine() ?? "";
            Console.Write($"Donner Le Prénom: ");
            firstN = Console.ReadLine() ?? "";
            Console.Write($"Donner Le numéro de télephone: ");
            tel= Console.ReadLine() ?? "";
            Console.Write("**********************\n");
            Compte cpt= new Compte(new Client(cin, firstN, lastN, tel));
            listeDesComptes.Add(cpt);
            nombreDeComptesCrees++;
            }
            choix();
        }
        static void choix() {
            Console.WriteLine("1.Afficher tous les comptes ");
            Console.WriteLine("2.Deposer un montant");
            Console.WriteLine("3.Retirer un montant");
            Console.WriteLine("4.Creer un autre compte");
            Console.WriteLine("0.Menu Principal");
            Console.WriteLine("***************************");
            Console.Write("Entrer votre choix : ");
            string choixMontant = Console.ReadLine();
                if(choixMontant == "1")
                {
                    AfficheComptes();
                }
                else if(choixMontant == "2")
                {
                    EffectuerDepot();
                }
                else if(choixMontant == "3")
                {
                    EffectuerRetrait();
                }
                else if(choixMontant == "0")
                {
                    Menu();
                }
                else if(choixMontant == "4")
                {
                    Compte();
                }
                else
                {
                Console.WriteLine ("Choix non valide");
                }
            }

            static void AfficheComptes()
                {
                Console.WriteLine("Liste des comptes :");
                foreach (Compte cpt in listeDesComptes)
                 {
                    Console.WriteLine($"Détails du compte : {cpt.Afficher()}");
                    Console.WriteLine("***************************");
                    Console.WriteLine("***************************");
                 }
                    Console.WriteLine($"Nombre total de compte creés : {nombreDeComptesCrees}");
                    Console.WriteLine("___________________");
                    choix();          
                }
           static void EffectuerDepot()
                {
                Console.Write("Enter le numero de la compte a debiter: ");
                int numeroCompte = int.Parse(Console.ReadLine());
                Console.Write("Donner le montant à déposer: ");
                float montant = float.Parse(Console.ReadLine());
                // Recherche du compte dans la liste
                Compte compte = listeDesComptes.Find(c => c.NumCompte == numeroCompte);
                if (compte != null)
                    {
                    // Effectuer le dépôt
                    compte.Crediter(montant);
                    Console.WriteLine($"Opération de dépôt bien effectuée. Nouveau solde : {compte.Solde}");
                    Console.WriteLine("***************************");
                    }
                else
                    {
                    Console.WriteLine("Numéro de compte invalide. Opération annulée.");
                    Console.WriteLine("***************************");
                    }
                 Compte();                    
                }
            static void EffectuerRetrait()
            {
            Console.Write("Enter le numero de la compte a debiter: ");
            int numeroCompte = int.Parse(Console.ReadLine());
            Console.Write("Donner le montant à retirer: ");
            float montant = float.Parse(Console.ReadLine());
            // Recherche du compte dans la liste
            Compte compte = listeDesComptes.Find(c => c.NumCompte == numeroCompte);
            if (compte != null)
                {
                // Effectuer le retrait
                if (compte.Solde >= montant)
                    {
                    compte.Debiter(montant);
                    Console.WriteLine($"Opération de retrait bien effectuée. Nouveau solde : {compte.Solde}");
                    Console.WriteLine("***************************");
                    }
                else
                {
                    Console.WriteLine("Solde insuffisant. Opération annulée.");
                    Console.WriteLine("***************************");
                }
                }
                else
                {
                    Console.WriteLine("Numéro de compte invalide. Opération annulée.");
                    Console.WriteLine("***************************");
                }
                 Compte();
            }}
    static void JeuxD()
    {
         String listeManches = "";

        Console.Write("Entrez le nombre de joueurs : ");
        Boolean a = Int32.TryParse(Console.ReadLine(), out Joueurs.nbJoueurs);

        Int32 num = 1;
        while(true) {
            Console.Write($"Entrez le nom du joueur n° {num} : ");
            String nom = Console.ReadLine() ?? "";

            Joueurs.AjouterJoueur(nom);

            if(num == Joueurs.nbJoueurs) {
                break;
            }
            num++;
        }

        while(true) {
            Console.WriteLine("\nScores actuels : ");
            Console.Write(Joueurs.FormaterScore());
            Console.ReadKey();
            Console.Write($@"
Choisissez une option : 
1 - Nouvelle manche 
2 - Voir l'historique des manches 
3 - Quitter 

Quel est votre choix ? ");

            String choix = Console.ReadLine() ?? "";

            if(choix.Equals("1")) {
                Manche thisManche = new Manche(Joueurs.nbJoueurs, Joueurs.listeJoueurs);
                thisManche.DateHeure = DateTime.Now.ToString();

                num = 1;
                String resultat = "";
                while(true) {
                    while(true) {
                        Console.Write($"\nTour de {Joueurs.GetNomJoueur(num-1)} -- Voulez-vous lancer le dé ? (y/n) ");
                        choix = Console.ReadLine() ?? "";

                        if(choix.Equals("y")) {
                            resultat += Joueurs.Jouer() + ",";
                            break;
                        } else if(choix.Equals("n")) {
                            resultat += 0 + ",";
                            break;
                        } 
                    }

                    if(num == Joueurs.nbJoueurs) {
                        break;
                    }
                    num++;
                }

                thisManche.SetPointsJoueurs(resultat);
                Joueurs.SetScoresJoueurs(thisManche.PointsJoueurs);
                thisManche.EtatScores = Joueurs.FormaterScore();

                listeManches += thisManche.InfoManche() + "\n";
            } else if(choix.Equals("2")) {
                Console.WriteLine(listeManches);
            } else if(choix.Equals("3")) {
                break;
            }
        }
    }
    static void Horloge()
    {
         Task task1;
        CancellationTokenSource cts;
        CancellationTokenSource cts2;
        List<Task> DisplayTZTask = new List<Task> {};
        List<TimeZoneInfo> suggestions;
        List<Alarm> alarmList = new List<Alarm> {};

        Console.WriteLine(@"Appli Horloge
Bienvenue !");

        while (true) {
            Console.Write(@"
Bienvenue dans le menu principale 
Choisissez une option : 
1 - Alarme 
2 - Horloge 
3 - Quitter 

Quel est votre choix ? ");

            string choice = Console.ReadLine() ?? "";

            while (choice == "1") {
                Console.Write(@"
Choisissez une option : 
1 - Voir les Alarmes actifs
2 - Creer une alarme 
3 - Retour menu principale

Quel est votre choix ? ");

                string alarmChoice = Console.ReadLine() ?? "";

                if (alarmChoice == "1") {
                    if (alarmList.Count == 0) {
                        Console.WriteLine("Il n'y a pas d'alarme actif.");
                        Console.ReadKey();
                    } else {
                        foreach(Alarm alarm in alarmList) {
                            Console.WriteLine(alarm.FormatAlarm());
                        }
                        Console.ReadKey();
                    }
                } else if (alarmChoice == "2") {
                    Console.WriteLine("Infos de la nouvelle alarme :");
                    Console.Write("Nom de l'alarme : ");
                    string alarmName = Console.ReadLine() ?? "";
                    Console.Write("Heure de l'alarme (hh:mm) : ");
                    bool a = TimeOnly.TryParse(Console.ReadLine(), out TimeOnly alarmTime);
                    Console.Write("Répéter l'alarme ? (O/n) ");
                    string toRepeat = Console.ReadLine() ?? "";
                    bool isRepeted;
                    if (toRepeat.ToLower()[0] == 'o') {
                        isRepeted = true;
                    } else {
                        isRepeted = false;
                    }

                    string alarmDays;

                    if (isRepeted) {
                        Console.Write("Jours de répétition (L/M/ME/J/V/S/D) : ");
                        alarmDays = Console.ReadLine() ?? "";
                    } else {
                        Console.Write("Jours de l'alarme (L/M/ME/J/V/S/D) : ");
                        alarmDays = Console.ReadLine() ?? "";
                    }

                    Alarm myAlarm = new Alarm(alarmName, alarmTime, alarmDays, isRepeted);
                    foreach (string day in myAlarm.AlarmDaysString)
                        Console.WriteLine(day);

                    alarmList.Add(myAlarm);
                } else if (alarmChoice == "3") {
                    break;
                }
            }

            while (choice == "2") {
                Console.WriteLine($"Heure actuelle ({ClockManagement.LocalTimeZoneId}).\n\n");

                Console.Write(@"
Choisissez une option : 
1 - Voir les Horloges actifs
2 - Ajouter une horloge 
3 - Retour menu principale

Quel est votre choix ? ");
                cts = new CancellationTokenSource();

                task1 = Task.Run(() => {
                    while (!cts.IsCancellationRequested) {
                        ConsoleManagement.SetLineAtFromHere(DateTime.Now.ToLongTimeString(), 7);

                        Thread.Sleep(1000);
                    }
                }, cts.Token);

                ExecuteTask(task1);
                string clockChoice = Console.ReadLine() ?? "";
                cts.Cancel();
                cts.Dispose();

                if (clockChoice == "1" && ClockManagement.TZToDisplay.Count > 0) {
                    cts2 = new CancellationTokenSource();

                    task1 = Task.Run(() => {
                        foreach (TimeZoneInfo timeZone in ClockManagement.TZToDisplay) {
                            Console.WriteLine("");
                            Console.WriteLine("");
                        }
                        while (!cts2.IsCancellationRequested) {
                            int i = 0;
                            foreach (TimeZoneInfo timeZone in ClockManagement.TZToDisplay) {
                                ConsoleManagement.SetLineAtFromHere(timeZone.DisplayName, 2*(ClockManagement.TZToDisplay.Count - i));
                                ConsoleManagement.SetLineAtFromHere(TimeZoneInfo.ConvertTimeFromUtc(DateTime.UtcNow, timeZone).ToLongTimeString(), 
                                2*(ClockManagement.TZToDisplay.Count - i) - 1);
                                i++;
                            }
                            
                            Thread.Sleep(1000);
                        }
                    }, cts2.Token);
                    Alarm a = alarmList[0];

                    ExecuteTask(task1);

                    Console.ReadKey();
                    cts2.Cancel();
                    cts2.Dispose();
                } else if (clockChoice == "1") {
                    Console.WriteLine("Il n'y a pas d'horloge à afficher.");
                    Console.ReadKey();
                } else if (clockChoice == "2") {
                    while (true) {
                        Console.WriteLine("Entrez votre recherche (Suggestions possibles, tapez tout de suite Entrer): ");
                        string searchString = Console.ReadLine() ?? "";

                        suggestions = ClockManagement.SearchSuggestion(searchString);

                        if(suggestions.Count > 1) {
                            Console.WriteLine("Suggestions : ");
                            foreach (TimeZoneInfo suggestion in suggestions)
                            {
                                Console.WriteLine(suggestion.DisplayName);
                            }
                        } else if (suggestions.Count == 0) {
                            Console.WriteLine("Aucune suggestion trouvée.");
                        } else if (!ClockManagement.TZToDisplay.Contains(suggestions[0])) {
                            Console.WriteLine(suggestions[0].DisplayName);
                            ClockManagement.TZToDisplay.Add(suggestions[0]);
                            break;
                        } else {
                            Console.WriteLine("L'horloge est déjà actif.");
                        }
                    }
                } else if (clockChoice == "3") {
                    break;
                }
            }

            if (choice == "3") {
                break;
            }
        }
    }

    static async void ExecuteTask(Task task) {
        await task;
    }

    }