using System;
using  TpExoCsharp;

class Joueur
{
    public string Nom { get; }
    public int Score { get; private set; }

    public Joueur(string nom)
    {
        Nom = nom;
    }

    public void MettreAJourScore(int points)
    {
        Score += points;
    }
}
